const path = require("path");
const apiMocker = require("mocker-api")

function resolve(dir) {
    return path.join(__dirname, dir);
}

module.exports = {
    css: {
        loaderOptions: {
            scss: {
                prependData: `
                    @import "@/assets/styles/global/_index.scss";
                `
            }
        }
    },
    chainWebpack: config => {
        // 配置别名
        config.resolve.alias
            .set("@", resolve("src"));
    },
    devServer: {
        // 期望使用mock接口,则将请求以 /api/xxx 开头
        before(app) {
            apiMocker(app, path.resolve("./mock/index.js"), {
                proxy: {},
                changeHost: true
            })
        }
    }
}
