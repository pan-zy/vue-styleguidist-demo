const path = require("path");
module.exports = {
    // 站点名称
    title: 'Vue Styleguidist 示例',
    // vue-styleguidist扫描规则
    components: 'src/components/**/[A-Z]*.vue',
    // 当前组件版本(用于自定义)
    version: '1.1.1',
    // build结果存放目录
    styleguideDir: 'styleguide-dist',
    // 设置每个章节在自己独立的页面
    pagePerSection: true,
    // 可以直接在界面上拷贝代码
    copyCodeButton: true,
    // 左侧的章节目录默认展开
    tocMode: 'expand',
    // props, emmit, methods说明默认展开
    usageMode: 'expand',
    // 示例默认展开
    exampleMode: 'expand',
    // 当props, emmit, methods不是来源于当前文件(实际可能来源于mixins)时, 是否显示其来源
    displayOrigins: true,
    // 用于改变，最顶部的文件路径(让使用者可以直接copy)
    getComponentPathLine(componentPath) {
        const name = path.basename(componentPath, '.vue')
        const dir = path.dirname(componentPath)
        return `import ${name} from '${dir}';`
    },
    // 左侧导航配置
    sections: [
        {
            name: '快速开始',
            content: 'docs/installation.md',
            description: 'The description for the installation section'
        },
        {
            name: "嵌套",
            sections: [
                {
                    name: '在线示例',
                    external: true,
                    href: 'http://example.com'
                }
            ]
        },
        {
            name: 'UI Components',
            content: 'docs/ui.md',
            components: 'src/components/**/[A-Z]*.vue'
        }
    ]
}
