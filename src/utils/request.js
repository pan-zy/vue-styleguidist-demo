import axios from 'axios'
import { Message } from 'element-ui'

// 响应成功的状态码
const sucStatusCodeArr = [200, 201]

// 请求拦截器
axios.interceptors.request.use(
  function (config) {
    console.log('执行请求拦截器...', config)
    if (config.url === '/api/reject') {
      return Promise.reject({ code: 502, msg: '请求被前端拦截器拒绝' })
    } else {
      return config
    }
  },
  function (error) {
    console.log('请求拦截器出错')
    return Promise.reject(error)
  }
)

/**
 * 处理响应状态码为2xx的响应结果
 * @param {*} response 响应内容
 * @returns
 */
const handler2xxStatusCodeResp = (response) => {
  // 获取到2xx响应
  console.log('响应拦截器:响应状态码是2xx', response)
  const { status: httpStatusCode, data: bizData } = response
  console.log('bizData', bizData instanceof Blob, bizData.type)
  if (sucStatusCodeArr.indexOf(httpStatusCode) >= 0) {
    if (bizData instanceof Blob) {
      // 如果响应数据是Blob, 那么就一定表示是文件下载
      if (bizData.type === 'application/octet-stream') {
        // 如果blob的类型是 application/octet-stream 表示下载成功
        return response
      } else if (bizData.type === 'application/json') {
        // 如果blob的类型是 application/json 说明后端有返回异常信息, 那么应该解析该数据
        var reader = new FileReader()
        reader.addEventListener('loadend', function (e) {
          console.log('e.target.result', e.target.result)
          const jsonObj = JSON.parse(e.target.result)
          Message({
            message: jsonObj.msg,
            type: 'error'
          })
        })
        reader.readAsText(bizData)
      }
      return Promise.reject({ code: 500, msg: '业务服务异常' })
    } else {
      if (bizData.code != 200 && bizData.code != 201) {
        // ==和===的区别是===先判断类型是否一致, 如果类型不一致, 则直接为false, 如果类型一致, 再判断值是否一致, ==是直接判断值是否一致
        Message({
          message: bizData.msg,
          type: 'error'
        })
        return Promise.reject({ code: 500, bizData })
      }
      return bizData
    }
  } else {
    throw { code: 500, msg: '业务服务异常' }
  }
}

const handlerNot2xxResp = (error) => {
  console.log('响应拦截器:响应状态码非2xx', error.code, error.response)
  const { data: bizData } = error.response
  if (error.response.status === 500) {
    Message({
      message: bizData.msg,
      type: 'error'
    })
  } else if (error.response.status === 403) {
    Message({
      message: bizData.msg,
      type: 'error'
    })
  } else if (error.response.status === 401) {
    Message({
      message: bizData.msg,
      type: 'error'
    })
  }
  return Promise.reject(error)
}

// 响应拦截器
axios.interceptors.response.use(handler2xxStatusCodeResp, handlerNot2xxResp)

export default axios
