# Vue Styleguidist demo

基于vue2使用Vue Styleguidist编写组件文档的示例项目，当前是生成的`HelloWorld`组件文档

## 命名与开发规范
css遵循ITCSS分层规范和BEM命名规范

## 目录说明

```
|styleguide.config.js                            # vue-styleguide配置文件
|docs                                            # 存放示例markdown的目录
|src
|--| assets                                      # 静态资源根目录
|--|--| svgs                                     # svg图标目录
|--|--| styles                                   # 样式根目录
|--|--|--| global                                # 全局公用的mixins和functions[这些mixins和functions与当前项目应该不存在任何形式的依赖关系], 这些都会在_index.scss中引入, 并导入webpack中
|--|--|--|-- functions                           # 全局公用的functions, 这些都会在_index.scss中引入, 并导入webpack中
|--|--|--|-- mixins                              # 全局公用的mixins, 这些都会在_index.scss中引入, 并导入webpack中
|--|--|--| project                               # 项目中用的样式
|--|--|--|-- tools                               # 项目中用到的mixins和functions
|--|--|--|-- theme                               # 项目中用到的主题样式
|--|--|--|-- settings                            # 项目中用到的变量
|--|--|--|-- reset                               # 对html元素的样式重置和增强
|--|--|--|-- object                              # 通过属性选择器的方式提供原子样式
|--|--| images                                   # 项目中用的图片
|--| components                                  # 存放项目中的组件
|--| router                                      # 存放项目中的路由相关配置
|--| store                                       # 存放项目中的vuex相关文件
|--| utils                                       # 存放一些帮助类
|--| views                                       # 存放界面文件
|--| App.vue                                     # 界面入口
|--| element-ui.js                               # element-ui按需加载配置
|--| main.js                                     # vue入口
```

## 命令

### 依赖安装
```
yarn install
```

### 热加载开发
```
yarn serve
```

### 生产环境编译打包
```
yarn build
```

### 修复代码格式化问题
```
yarn lint
```

### 生产打包结果分析
```
yarn analyzer
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
